#ifndef ANIMABLE_H
#define ANIMABLE_H

#include <glm.hpp>

class Animable
{
public:
    virtual void setPosition(const glm::vec3& position) = 0;
    virtual void setDirection(const glm::dquat& direction) = 0;
    // virtual void setScale(double scale) = 0;

    virtual bool translatable() const = 0;
    virtual bool rotable() const = 0;
    virtual bool scalable() const = 0;
};

#endif // ANIMABLE_H
