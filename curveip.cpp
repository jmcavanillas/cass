#include "curveip.h"
#include <iostream>

glm::vec3 CurveIP::basic_interpolation(double t)
{
    return path_.getWithTValue(t);
}

CurveIP::CurveIP() :
    path_(glm::vec3(0), glm::vec3(-30, 30 , 0), glm::vec3(30,30,0), glm::vec3(30, 10, 0))
{

}

glm::vec3 CurveIP::interpolate(double t)
{
    return basic_interpolation(t);
}

glm::vec3 CurveIP::interpolateS(double s)
{
   return path_.getWithSValue(s);
}
