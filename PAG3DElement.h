#pragma once

#include <glm.hpp>
#include "animable.h"
#include "PAGMaterial.h"
#include "PAGShaderProgram.h"

class PAG3DElement : public Animable
{
    protected:
        glm::mat4 modelMatrix_;
		PAGMaterial material_;

	public: 

        virtual void setPosition(const glm::vec3& position);
        virtual void setDirection(const glm::dquat& direction);
        // virtual void setScale(double scale);

        virtual bool scalable() const;
        virtual bool rotable() const;
        virtual bool translatable() const;

        virtual void setModelMatrix(const glm::mat4& model_matrix);
		virtual void setMaterial(const PAGMaterial& material);

        virtual glm::mat4 getModelMatrix();
		virtual PAGMaterial getMaterial();

        virtual void drawAsPoints(const PAGShaderProgram& shader, const glm::mat4& vp_matrix, const glm::mat4& v_matrix) = 0;
        virtual void drawAsLines(const PAGShaderProgram& shader, const glm::mat4& vp_matrix, const glm::mat4& v_matrix) = 0;
        virtual void drawAsTriangles(const PAGShaderProgram& shader, const glm::mat4& vp_matrix, const glm::mat4& v_matrix) = 0;
};

