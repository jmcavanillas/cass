#include "PAGTexture.h"
#include <iostream>
#include "lodepng.h"

void PAGTexture::inverseImg(std::vector<unsigned char>& image, unsigned width, unsigned height)
{
	unsigned char* imgPtr = &image[0];
	int numColorComponents = 4;
	int wInc = width * 4; // width in char
	unsigned char* top = nullptr;
	unsigned char* bot = nullptr;
	unsigned char temp = 0;

	for (int i = 0; i < height / 2; ++i)
	{
		top = imgPtr + i * wInc;
		bot = imgPtr + (height - i - 1) * wInc;
		for (int j = 0; j < wInc; ++j)
		{
			temp = *top;
			*top = *bot;
			*bot = temp;
			++top;
			++bot;
		}
	}
}

bool PAGTexture::fileExists(const std::string & fileName)
{
	struct stat info;
	int ret = -1;
	ret = stat(fileName.c_str(), &info);
	return 0 == ret;
}

PAGTexture::PAGTexture()
{
}


GLuint PAGTexture::addTexture(const std::string & fileName)
{
	if (!fileExists(fileName))
		return 0;

	std::vector<unsigned char> image;
	unsigned width, height;
	
	unsigned error = lodepng::decode(image, width, height, fileName);

	if (error)
	{
		std::cout << fileName << " cannot be loaded" << std::endl;
		return 0;
	}

	inverseImg(image, width, height);

	GLuint handler_ = 0;
	glGenTextures(1, &handler_);
	glBindTexture(GL_TEXTURE_2D, handler_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());

	if (handler_)
		handlers_.push_back(handler_);

	return handler_;
}

void PAGTexture::activate()
{
	for (int i = 0; i < handlers_.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, handlers_[i]);
	}
}

void PAGTexture::setTextureRepeat(bool repeat)
{
	for (GLuint handler_ : handlers_)
	{
		glBindTexture(GL_TEXTURE_2D, handler_);
		if (repeat)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}
		else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}
	}
}

void PAGTexture::setTextureMinFilter(int filter)
{
	for (GLuint handler_ : handlers_)
	{
		glBindTexture(GL_TEXTURE_2D, handler_);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	}
}

void PAGTexture::setTextureMaxFilter(int filter)
{
	for (GLuint handler_ : handlers_)
	{
		glBindTexture(GL_TEXTURE_2D, handler_);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	}
}

void PAGTexture::setMirrorRepeat(bool mirror_repeat)
{
	for (GLuint handler_ : handlers_)
	{
		glBindTexture(GL_TEXTURE_2D, handler_);
		if (mirror_repeat)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}
	}
}

PAGTexture::~PAGTexture()
{
	glDeleteTextures(handlers_.size(), handlers_.data());
}
