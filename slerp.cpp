#include "slerp.h"

#include <glm/gtc/quaternion.hpp>

glm::dquat Slerp::basic_interpolation(const glm::dquat &q1, const glm::dquat &q2, double t)
{
    glm::dquat n_q1 = glm::normalize(q1);
    glm::dquat n_q2 = glm::normalize(q2);

    double dot = glm::dot(n_q1, n_q2);

    // Con el producto escalar averiguamos si es o no el camino mas corto
    if (dot < 0.0f)
    {
        n_q1 = -n_q1;
        dot = -dot;
    }

    const double DOT_THRESHOLD = 0.9995;

    if (dot > DOT_THRESHOLD)
    {
        glm::dquat result = t * n_q1 + (1-t) * n_q2;
        result = glm::normalize(result);
        return result;
    }

    double theta = acos(dot);
    double sin_theta = sin(theta);

    double s1 = (sin(1-t) * theta) / sin_theta;
    double s2 = sin_theta * t / sin_theta;

    return glm::normalize(s1 * n_q1 + s2 * n_q2);
}

Slerp::Slerp()
{

}

Slerp::Slerp(const std::vector<glm::dquat> &points, const std::vector<double> &times)
{
    dirs_ = points;
    times_ = times;
}

glm::quat Slerp::interpolate(double t) const
{
    t = (t * 1000);

    if (t >= times_.back()) return basic_interpolation(dirs_[dirs_.size()-2], dirs_.back(), t/1000);
    if (t <= times_.front()) return basic_interpolation(dirs_[0], dirs_[1], t/1000);

    for (unsigned i = 0; i < times_.size() - 1; ++i)
    {
        // Buscamos el intervalo en el que esta
        if (t <= times_[i+1] && t >= times_[i])
        {
            //Calculamos el sub_t en el que esta
            double sub_t = (t - times_[i]) / (times_[i+1] - times_[i]);
            // LLamamos a la interpolación básica
            return basic_interpolation(dirs_[i], dirs_[i+1], sub_t);
        }
    }

    return dirs_.back();
}
