#include "PAG3DElement.h"

#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/quaternion.hpp>

#include <iostream>

void PAG3DElement::setPosition(const glm::vec3 &position)
{
    modelMatrix_ = glm::column(modelMatrix_, 3, glm::vec4(position, 1));
}

void PAG3DElement::setDirection(const glm::dquat &direction)
{
    glm::dmat3 dir_matrix = glm::mat4_cast(direction);
    glm::mat4 model_matrix = getModelMatrix();
    glm::mat4 new_model_matrix =
            glm::mat4(
                dir_matrix[0][0], dir_matrix[0][1], dir_matrix[0][2], model_matrix[0][3],
                dir_matrix[1][0], dir_matrix[1][1], dir_matrix[1][2], model_matrix[1][3],
                dir_matrix[2][0], dir_matrix[2][1], dir_matrix[2][2], model_matrix[2][3],
                model_matrix[3][0], model_matrix[3][1], model_matrix[3][2], model_matrix[3][3]
            );

    setModelMatrix(new_model_matrix);
}

void PAG3DElement::setModelMatrix(const glm::mat4 & model_matrix)
{
	modelMatrix_ = model_matrix;
}

void PAG3DElement::setMaterial(const PAGMaterial & material)
{
	material_ = material;
}

glm::mat4 PAG3DElement::getModelMatrix()
{
	return modelMatrix_;
}

PAGMaterial PAG3DElement::getMaterial()
{
	return material_;
}

bool PAG3DElement::scalable() const
{
    return true;
}

bool PAG3DElement::rotable() const
{
    return true;
}

bool PAG3DElement::translatable() const
{
    return true;
}
