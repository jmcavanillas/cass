#include "cubicbeziercurve3d.h"

glm::mat4 CubicBezierCurve3D::M = glm::mat4(-1, 3, -3, 1, 3, -6, 3, 0, -3, 3, 0, 0, 1, 0, 0, 0);

CubicBezierCurve3D::CubicBezierCurve3D()
{
    point1_ = glm::vec3(0);
    point2_ = glm::vec3(1);
    point3_ = glm::vec3(3);
    point4_ = glm::vec3(0);
    fetch_polyline(1000);
}

CubicBezierCurve3D::CubicBezierCurve3D(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& p4)
{
    point1_ = p1;
    point2_ = p2;
    point3_ = p3;
    point4_ = p4;
    fetch_polyline(1000);
}

double CubicBezierCurve3D::searchArc(double s) const
{
    int round = static_cast<int>(s);
    double decimal = s - round;
    double t = 0;
    for(int i = 0; i <= frames_; ++i)
    {
        if(std::abs(decimal) <= sLine_[i])
        {
            t = static_cast<double>(i)/frames_;
            break;
        }
        t = 1;
    }

    return round + t;
}

void CubicBezierCurve3D::fetch_polyline(unsigned frames)
{
    frames_ = frames;
    std::vector<float> arcDistances;
    arcDistances.push_back(0);
    glm::vec3 prev_point = getWithTValue(0);
    for (int i = 0; i <= frames_; ++i)
    {
        glm::vec3 point = getWithTValue(static_cast<double>(i)/frames_);
        polyline_.push_back(point);
        arcDistances.push_back(arcDistances.back() + glm::distance(prev_point, point));
        prev_point = point;
    }

    for(int i = 0; i <= frames_; ++i)
        sLine_.push_back(arcDistances[i]/arcDistances.back());
}


glm::vec3 CubicBezierCurve3D::getWithTValue(double t)
{
//    if(t > 1) t = 1;
//    if(t < 0) t = 0;

    glm::vec4 GX = glm::vec4(point1_.x, point2_.x, point3_.x, point4_.x);
    glm::vec4 GY = glm::vec4(point1_.y, point2_.y, point3_.y, point4_.y);
    glm::vec4 GZ = glm::vec4(point1_.z, point2_.z, point3_.z, point4_.z);
    glm::vec4 T = glm::vec4(t*t*t, t*t, t, 1);

    glm::vec4 RX = GX * M * T;
    glm::vec4 RY = GY * M * T;
    glm::vec4 RZ = GZ * M * T;

    float x = RX.x + RX.y + RX.z + RX.w;
    float y = RY.x + RY.y + RY.z + RY.w;
    float z = RZ.x + RZ.y + RZ.z + RZ.w;

    return glm::vec3(x,y,z);
}

glm::vec3 CubicBezierCurve3D::getWithSValue(double s)
{
    return getWithTValue(searchArc(s));
}
