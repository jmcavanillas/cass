#include "animation.h"
#include <iostream>
#include <glm/gtc/quaternion.hpp>

bool Animation::anPosition() const
{
    return anPosition_;
}

void Animation::setAnPosition(bool anPosition)
{
    anPosition_ = anPosition;
}

bool Animation::anRotation() const
{
    return anRotation_;
}

void Animation::setAnRotation(bool anRotation)
{
    anRotation_ = anRotation;
}

bool Animation::anScale() const
{
    return anScale_;
}

void Animation::setAnScale(bool anScale)
{
    anScale_ = anScale;
}

Animation::Animation() : speed_(glm::vec2(0,0), glm::vec2(0.333,5), glm::vec2(1,-2), glm::vec2(1,0.8)),
    easing_(glm::vec2(0,0), glm::vec2(0.785,0.135), glm::vec2(0.15,0.86), glm::vec2(1,1))
{
    std::vector<glm::vec3> points = {glm::vec3(0,20,-10), glm::vec3(10,20,20), glm::vec3(20,20,20), glm::vec3(10,20,25)};
    std::vector<double> times = {0, 333, 777, 1000};
    std::vector<glm::dquat> quats = {glm::dquat(glm::vec3(0,0,0)),
                                     glm::dquat(glm::vec3(0, 0.4, glm::radians(90.))),
                                     glm::dquat(glm::vec3(0.6, 0, glm::radians(180.))),
                                     glm::dquat(glm::vec3(0, -0.4, glm::radians(270.))),
                                     glm::dquat(glm::vec3(0, 0, glm::radians(0.)))
                                    };
    std::vector<double> times_slerp = {0, 250, 500, 750, 1000};
    posInterpolation_ = Lerp<glm::vec3>(points, times);
    SphericalInterpolation_ = Slerp(quats, times_slerp);
    object_ = nullptr;
    timer_ = new QTimer();
    connect(timer_,SIGNAL(timeout()), this, SLOT(update()));
    interval_ = 10;
    tickCount_ = 0;
    dur_ = 3000;

    usingSpeed_ = false;
}

void Animation::start()
{
    tickCount_ = 0;
    progress_ = 0;
    timer_->start(interval_);
}

void Animation::setObject(Animable *object)
{
    object_ = object;
}

void Animation::update()
{
    double t = static_cast<double>(tickCount_)/dur_;

    if(usingSpeed_)
    {
        float sp = speed_.getPoint(t).y;

        double f = 0.001;
        progress_ += sp;
        object_->setPosition(posInterpolation_.interpolate(progress_*f));
        tickCount_ += interval_;
        if (progress_*f >= 1)
            timer_->stop();
    } else {
        float s = easing_.getTForX(t).y;
        object_->setPosition(curveInterpolation_.interpolateS(s));
        object_->setDirection(SphericalInterpolation_.interpolate(s));
        tickCount_ += interval_;
        if (t >= 1)
            timer_->stop();
    }

    //std::cout << t << ": " << speed_.getPoint(t).y << std::endl;

}

Animation::~Animation()
{
    delete timer_;
}
