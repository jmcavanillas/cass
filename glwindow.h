#ifndef WINDOW_H
#define WINDOW_H

#include <GL/glew.h>

#include <QOpenGLWindow>
#include <QOpenGLFunctions>

#include "PAGRenderer.h"

class GLWindow : public QOpenGLWindow,
        protected QOpenGLFunctions
{
    Q_OBJECT

    bool mouseLBPressed_;
    QPoint	mousePressedPos_;

public:
    GLWindow();
    ~GLWindow();

    // Paint Events
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void teardownGL();

    // Mouse Control Events
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    // Keyboard Control Events
    void keyPressEvent(QKeyEvent *event);

private:
    // Private Helpers
    void printContextInformation();
};

#endif // WINDOW_H
