#include <QApplication>
#include <iostream>

#include "glwindow.h"

//extern "C" {
//    _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
//}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);


    // Set OpenGL Version information
    // Note: This format must be set before show() is called.
    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setVersion(4,1);

    // Set the window up
    GLWindow window;
    window.setFormat(format);
    window.setTitle("CASS - Cavanillas Animation Small Studio");
    window.resize(QSize(800, 600));
    window.show();

    return app.exec();
}
