#include "cubicbeziercurve2d.h"

glm::mat4 CubicBezierCurve2D::M = glm::mat4(-1, 3, -3, 1, 3, -6, 3, 0, -3, 3, 0, 0, 1, 0, 0, 0);

CubicBezierCurve2D::CubicBezierCurve2D()
{
    point1_ = glm::vec2(1);
    point2_ = glm::vec2(1);
    point3_ = glm::vec2(1);
    point4_ = glm::vec2(1);
    fetch_polyline(1000);
}

CubicBezierCurve2D::CubicBezierCurve2D(float v1, float v2, float v3, float v4)
{
    point1_ = glm::vec2(0);
    point2_ = glm::vec2(v1, v2);
    point3_ = glm::vec2(v3, v4);
    point4_ = glm::vec2(1);
    fetch_polyline(1000);
}

CubicBezierCurve2D::CubicBezierCurve2D(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3, const glm::vec2& p4)
{
    point1_ = p1;
    point2_ = p2;
    point3_ = p3;
    point4_ = p4;
    fetch_polyline(1000);
}

void CubicBezierCurve2D::fetch_polyline(unsigned frames)
{
    frames_ = frames;
    std::vector<float> arcDistances;
    arcDistances.push_back(0);
    glm::vec2 prev_point = getPoint(0);
    for (int i = 0; i <= frames_; ++i)
    {
        glm::vec2 point = getPoint(static_cast<double>(i)/frames_);
        polyline_.push_back(point);
        arcDistances.push_back(arcDistances.back() + glm::distance(prev_point, point));
        prev_point = point;
    }

    for(int i = 0; i <= frames_; ++i)
        sLine_.push_back(arcDistances[i]/arcDistances.back());

}

double CubicBezierCurve2D::searchForX(double s) const
{
    for(int i = 0; i <= frames_; ++i)
        if(s < polyline_[i].x)
            return static_cast<double>(i)/frames_;

    return 1;
}

glm::vec2 CubicBezierCurve2D::getPoint(double t) const
{
//    if(t > 1) t = 1;
//    if(t < 0) t = 0;

    glm::vec4 GX = glm::vec4(point1_.x, point2_.x, point3_.x, point4_.x);
    glm::vec4 GY = glm::vec4(point1_.y, point2_.y, point3_.y, point4_.y);
    glm::vec4 T = glm::vec4(t*t*t, t*t, t, 1);

    glm::vec4 RX = GX * M * T;
    glm::vec4 RY = GY * M * T;

    float x = RX.x + RX.y + RX.z + RX.w;
    float y = RY.x + RY.y + RY.z + RY.w;

    return glm::vec2(x,y);
}

glm::vec2 CubicBezierCurve2D::getTForX(double x) const
{
//    // Metodo Newton raphson
//    double guessT = x;
//    for (int i = 0; i < 8; ++i)
//    {
//        double currentSlope = getSlope(guessT);
//        if(currentSlope == 0) return guessT;
//        double currentX = getVal(guessT) - x;
//        guessT -= currentX / currentSlope;
//    }
//    return guessT;
    double t = searchForX(x);
    return getPoint(t);
}
