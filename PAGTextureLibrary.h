#pragma once

#include <string>
#include <map>
#include "PAGTexture.h"

class PAGTextureLibrary
{
	private:
		static PAGTextureLibrary* instance_;

		std::map<std::string, PAGTexture*> textures_;

		PAGTextureLibrary();
	public:
		static PAGTextureLibrary* getInstance();

		PAGTexture* newTexture(const std::string& name);
		PAGTexture* operator[](const std::string& name);

		virtual ~PAGTextureLibrary();
};

