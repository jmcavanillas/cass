#include "PAGTextureLibrary.h"

PAGTextureLibrary* PAGTextureLibrary::instance_ = nullptr;

PAGTextureLibrary::PAGTextureLibrary()
{
	PAGTexture* texture = new PAGTexture();
	texture->addTexture("neutral.png");
	texture->addTexture("normal_neutral.png");
	texture->addTexture("el_neutral.png");
	textures_["None"] = texture;
}


PAGTextureLibrary * PAGTextureLibrary::getInstance()
{
	if (!instance_)
		instance_ = new PAGTextureLibrary();

	return instance_;
}

PAGTexture * PAGTextureLibrary::newTexture(const std::string& name)
{
	PAGTexture* texture = new PAGTexture();
	textures_.insert(std::pair<std::string, PAGTexture*>(name, texture));
	return texture;
}

PAGTexture * PAGTextureLibrary::operator[](const std::string& name)
{
	auto result = textures_.find(name);
	if (result == textures_.end())
		return nullptr;

	return result->second;
}

PAGTextureLibrary::~PAGTextureLibrary()
{
	for (std::pair<std::string, PAGTexture*> texture : textures_)
		delete texture.second;
}
