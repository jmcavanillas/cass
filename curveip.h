#ifndef LINIPONCURVES_H
#define LINIPONCURVES_H

#include <glm.hpp>
#include <vector>
#include "cubicbeziercurve2d.h"
#include "cubicbeziercurve3d.h"

class CurveIP
{
private:
    glm::vec3 basic_interpolation(double t);


    unsigned frames_;
    CubicBezierCurve3D path_;


public:
    CurveIP();
    CurveIP(const glm::vec3& p1,const glm::vec3& p2,const glm::vec3& p3,const glm::vec3& p4);

    glm::vec3 interpolate(double t);
    glm::vec3 interpolateS(double s);
};

#endif // LINIPONCURVES_H
