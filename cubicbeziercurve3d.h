#ifndef CUBICBEZIERCURVE3D_H
#define CUBICBEZIERCURVE3D_H

#include <glm.hpp>
#include <vector>

class CubicBezierCurve3D
{
private:
    static glm::mat4 M;

    glm::vec3 point1_;
    glm::vec3 point2_;
    glm::vec3 point3_;
    glm::vec3 point4_;

    unsigned int frames_;
    std::vector<glm::vec3> polyline_;
    std::vector<double> sLine_;

    double searchArc(double s) const;
    void fetch_polyline(unsigned frames);

public:
    CubicBezierCurve3D();
    CubicBezierCurve3D(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& p4);

    glm::vec3 getWithTValue(double t);
    glm::vec3 getWithSValue(double s);

};

#endif // CUBICBEZIERCURVE3D_H
