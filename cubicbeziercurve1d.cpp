#include "cubicbeziercurve1d.h"
#include <iostream>

glm::mat4 CubicBezierCurve1D::M = glm::mat4(-1, 3, -3, 1, 3, -6, 3, 0, -3, 3, 0, 0, 1, 0, 0, 0);

double CubicBezierCurve1D::getSlope(double t) const
{
    return (val2_ - val1_) * std::pow(1-t, 2) + (val3_-val2_) * 2 * t * (1-t) + (val4_ - val3_) * std::pow(t, 2);
}

void CubicBezierCurve1D::fetchPolyline(unsigned frames)
{
    frames_ = frames;
    std::vector<float> arcDistances;
    arcDistances.push_back(0);
    double prev_point = getVal(0);
    for (int i = 0; i <= frames_; ++i)
    {
        double point = getVal(static_cast<double>(i)/frames_);
        arcDistances.push_back(arcDistances.back() + (std::abs(point- prev_point)));
        prev_point = point;
    }

    for(int i = 0; i <= frames_; ++i)
        arcLengths_.push_back(arcDistances[i]/arcDistances.back());
}

CubicBezierCurve1D::CubicBezierCurve1D()
{
    val1_ = 0;
    val2_ = 1;
    val3_ = 3;
    val4_ = 0;
}

CubicBezierCurve1D::CubicBezierCurve1D(double p1, double p2, double p3, double p4)
{
    val1_ = p1;
    val2_ = p2;
    val3_ = p3;
    val4_ = p4;
    fetchPolyline(1000);
}

double CubicBezierCurve1D::searchArc(double s) const
{
    for(int i = 0; i <= frames_; ++i)
        if(s < arcLengths_[i])
            return static_cast<double>(i)/frames_;

    return 1;
}

double CubicBezierCurve1D::getVal(double t) const
{
    if(t > 1) t = 1;
    if(t < 0) t = 0;

    glm::vec4 G = glm::vec4(val1_, val2_, val3_, val4_);
    glm::vec4 T = glm::vec4(t*t*t, t*t, t, 1);

    glm::vec4 R = G * M * T;

    float r = R.x + R.y + R.z + R.w;

    return r;
}

double CubicBezierCurve1D::getTForX(double x) const
{
//    // Metodo Newton raphson
//    double guessT = x;
//    for (int i = 0; i < 8; ++i)
//    {
//        double currentSlope = getSlope(guessT);
//        if(currentSlope == 0) return guessT;
//        double currentX = getVal(guessT) - x;
//        guessT -= currentX / currentSlope;
//    }
//    return guessT;
    double t = searchArc(x);
    std::cout << t << std::endl;
    return getVal(t);
}
