#-------------------------------------------------
#
# Project created by QtCreator 2019-02-13T15:44:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Animacion3D
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

INCLUDEPATH += $$PWD/glm


LIBS += -lOpengl32

unix|win32: LIBS += -LC:/Users/Javier/Desktop/glwrappers/glew-2.1.0/lib/Release/x64/ -lglew32

INCLUDEPATH += C:/Users/Javier/Desktop/glwrappers/glew-2.1.0/include
DEPENDPATH += C:/Users/Javier/Desktop/glwrappers/glew-2.1.0/include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    shaders/ambDisplacementShader-frag.glsl \
    shaders/ambDisplacementShader-vert.glsl \
    shaders/ambientLightAds-frag.glsl \
    shaders/ambientLightAds-vert.glsl \
    shaders/dirBumpShader-frag.glsl \
    shaders/dirBumpShader-vert.glsl \
    shaders/dirDisplacementShader-frag.glsl \
    shaders/dirDisplacementShader-vert.glsl \
    shaders/dirLightAds-frag.glsl \
    shaders/dirLightAds-vert.glsl \
    shaders/dirLightAdsTex-frag.glsl \
    shaders/dirLightAdsTex-vert.glsl \
    shaders/fogDisplacementShader-frag.glsl \
    shaders/fogDisplacementShader-vert.glsl \
    shaders/fogShader-frag.glsl \
    shaders/fogShader-vert.glsl \
    shaders/lineShader-frag.glsl \
    shaders/lineShader-vert.glsl \
    shaders/magicShader-frag.glsl \
    shaders/magicShader-vert.glsl \
    shaders/normalsAsColor-frag.glsl \
    shaders/normalsAsColor-vert.glsl \
    shaders/pointBumpShader-frag.glsl \
    shaders/pointBumpShader-vert.glsl \
    shaders/pointDisplacementShader-frag.glsl \
    shaders/pointDisplacementShader-vert.glsl \
    shaders/pointLightAds-frag.glsl \
    shaders/pointLightAds-vert.glsl \
    shaders/pointLightAdsTex-frag.glsl \
    shaders/pointLightAdsTex-vert.glsl \
    shaders/pointShader-frag.glsl \
    shaders/pointShader-vert.glsl \
    shaders/spotLightAds-frag.glsl \
    shaders/spotLightAds-vert.glsl \
    shaders/spotLightAdsTex-frag.glsl \
    shaders/spotLightAdsTex-vert.glsl \
    shaders/spotlightBumpShader-frag.glsl \
    shaders/spotlightBumpShader-vert.glsl \
    shaders/spotlightDisplacementShader-frag.glsl \
    shaders/spotlightDisplacementShader-vert.glsl \
    shaders/tangentsAsColor-frag.glsl \
    shaders/tangentsAsColor-vert.glsl \
    shaders/textCoordsAsColors-frag.glsl \
    shaders/textCoordsAsColors-vert.glsl \
    shaders/triangleFaceShader-frag.glsl \
    shaders/triangleFaceShader-vert.glsl \
    textures/el_neutral.png \
    textures/moon_el_map_2.png \
    textures/moon_normals_2_720.png \
    textures/neutral.png \
    textures/normal_neutral.png \
    textures/tie_fighter.png \
    textures/tie_fighter_el.png \
    textures/tie_fighter_normal.png \
    textures/tie_fighter_wing.png \
    textures/tie_fighter_wing_el.png \
    textures/tie_fighter_wing_normal.png \
    textures/uv_checker large.png \
    textures/uv_checker vHalf.png \
    textures/uv_checker vQuart.png \
    textures/wing_lid.png \
    textures/wing_lid_el.png \
    textures/wing_lid_normal.png \
    assets/myfile.txt

HEADERS += \
    glwindow.h \
    lodepng.h \
    MaterialLibrary.h \
    PAG3DElement.h \
    PAG3DGroup.h \
    PAGAmbientLightApplier.h \
    PAGCamera.h \
    PAGDirLightApplier.h \
    PAGEnumeration.h \
    PAGLightApplier.h \
    PAGLightSource.h \
    PAGMaterial.h \
    PAGPlane.h \
    PAGPointLightApplier.h \
    PAGRenderer.h \
    PAGRevolutionObject.h \
    PAGShaderCollection.h \
    PAGShaderProgram.h \
    PAGSpotLightApplier.h \
    PAGStructures.h \
    PAGSubdivisionProfile.h \
    PAGTexture.h \
    PAGTextureLibrary.h \
    PAGVao.h \
    resource.h \
    sceneObjects.h \
    linealinterpol.h \
    cubicbeziercurve2d.h \
    cubicbeziercurve3d.h \
    curveip.h \
    cubicbeziercurve1d.h \
    animation.h \
    animable.h \
    slerp.h

SOURCES += \
    glwindow.cpp \
    lodepng.cpp \
    main.cpp \
    MaterialLibrary.cpp \
    PAG3DElement.cpp \
    PAG3DGroup.cpp \
    PAGAmbientLightApplier.cpp \
    PAGCamera.cpp \
    PAGDirLightApplier.cpp \
    PAGLightSource.cpp \
    PAGMaterial.cpp \
    PAGPlane.cpp \
    PAGPointLightApplier.cpp \
    PAGRenderer.cpp \
    PAGRevolutionObject.cpp \
    PAGShaderCollection.cpp \
    PAGShaderProgram.cpp \
    PAGSpotLightApplier.cpp \
    PAGSubdivisionProfile.cpp \
    PAGTexture.cpp \
    PAGTextureLibrary.cpp \
    PAGVao.cpp \
    sceneObjects.cpp \
    cubicbeziercurve2d.cpp \
    cubicbeziercurve3d.cpp \
    curveip.cpp \
    cubicbeziercurve1d.cpp \
    animation.cpp \
    slerp.cpp

FORMS +=
