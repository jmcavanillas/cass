#include "glwindow.h"

#include <QDebug>
#include <QString>
#include <QMouseEvent>

#include <PAGEnumeration.h>

GLWindow::GLWindow() : QOpenGLWindow()
{
    mouseLBPressed_ = false;
    mousePressedPos_ = QPoint(0,0);
}

GLWindow::~GLWindow()
{
    makeCurrent();
    teardownGL();
}

/*******************************************************************************
 * OpenGL Events
 ******************************************************************************/

void GLWindow::initializeGL()
{
    // Initialize OpenGL Backend
    initializeOpenGLFunctions();
    printContextInformation();

    // Connect signal for Vsync loop
    connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));

    // Set global information
    glewExperimental = true;

    if (glewInit() != GLEW_OK)
        std::cout << "Failed to initialize GLEW" << std::endl;

    PAGRenderer::getInstance()->prepareOpenGL();
}

void GLWindow::resizeGL(int width, int height)
{
    PAGRenderer::getInstance()->change_viewport_size(width, height);
}

void GLWindow::paintGL()
{
    // Clear
    glClear(GL_COLOR_BUFFER_BIT);

    if(mouseLBPressed_)
    {
        float distance = QWindow::mapFromGlobal(cursor().pos()).x() - mousePressedPos_.x();
        PAGRenderer::getInstance()->orbit(distance/20);
    }

    PAGRenderer::getInstance()->refresh();

}

void GLWindow::teardownGL()
{
    // Currently we have no data to teardown
}

void GLWindow::mouseMoveEvent(QMouseEvent *event)
{

}

void GLWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
        mouseLBPressed_ = true;
    mousePressedPos_ = event->pos();
}

void GLWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (!(event->buttons() & Qt::LeftButton))
        mouseLBPressed_ = false;
}

void GLWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key::Key_W)
        PAGRenderer::getInstance()->setView(PAG_LINE_VIEW);

    if (event->key() == Qt::Key::Key_T)
        PAGRenderer::getInstance()->setView(PAG_TRIANGLE_VIEW);

    if (event->key() == Qt::Key::Key_P)
        PAGRenderer::getInstance()->setView(PAG_POINT_VIEW);
}

/*******************************************************************************
 * Private Helpers
 ******************************************************************************/

void GLWindow::printContextInformation()
{
    QString glType;
    QString glVersion;
    QString glProfile;

    // Get Version Information
    glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

    // Get Profile Information
#define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
    switch (format().profile())
    {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
    }
#undef CASE

    // qPrintable() will print our QString w/o quotes around it.
    qDebug() << qPrintable(glType) << qPrintable(glVersion) << "(" << qPrintable(glProfile) << ")";
}
