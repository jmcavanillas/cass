#version 400

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 3) in vec2 vTexCoord;

uniform float dFactor;
uniform sampler2D TexSamplerEl;

uniform mat4 mModelViewProj;
uniform mat4 mModelView;

out vec3 position;

void main()
{
	float displacement = texture(TexSamplerEl, vTexCoord).r;
	vec3 posAux = vPosition + (vNormal * displacement * dFactor);

	position = vec3(mModelView * vec4(posAux, 1.0));
	gl_Position = mModelViewProj * vec4(posAux, 1.0);
}