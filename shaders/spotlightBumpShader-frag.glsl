#version 400
#define M_PI 3.1415926535897932384626433832795

in vec3 position;
in vec3 lDir;
in vec3 vDir;
in vec3 lgDir;
in vec2 texCoord;

uniform vec4 mtDiffuse;
uniform vec4 mtSpecular;
uniform float mtShininess;

uniform vec3 lgDirection;
uniform vec3 lgDiffuse;
uniform vec3 lgSpecular;
uniform float lgScattAng;
uniform float lgScattExp;

uniform sampler2D TexSamplerNormal;
uniform sampler2D TexSamplerColor;

layout (location = 0) out vec4 fragColor;

vec3 diffuseAndSpecular(vec4 texColor, vec4 normal)
{
	vec4 mtDiffuse = texColor;
	vec3 l = normalize(lDir);
	vec3 d = normalize(lgDir);

	float cosGamma = cos(lgScattAng * M_PI / 180);
	float spotFactor = 1.0;

	if (dot(-l, d) < cosGamma) 
	{
		spotFactor = 0.0;
	} 
	else {
		spotFactor = pow(dot(-l, d), lgScattExp);
	}

	vec3 n = normalize(vec3 (normal));
	vec3 v = normalize(vDir);
	vec3 r = reflect(-l, n);

	vec3 diffuse = (lgDiffuse * vec3(mtDiffuse) * max(dot(l,n), 0.0));
	vec3 specular = (lgSpecular * vec3(mtSpecular) * pow(max(dot(r,v), 0.0), mtShininess));

	return spotFactor * (diffuse + specular);
}

void main() 
{
	vec4 texColor = mtDiffuse * texture(TexSamplerColor, texCoord);
	vec4 normal = (2.0 * texture(TexSamplerNormal, texCoord)) - 1.0;
	fragColor = vec4(diffuseAndSpecular(texColor, normal), 1);
}