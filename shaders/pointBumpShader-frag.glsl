#version 400

in vec3 position;
in vec3 lDir;
in vec3 vDir;
in vec2 texCoord;

uniform vec4 mtDiffuse;
uniform vec4 mtSpecular;
uniform float mtShininess;

uniform vec3 lgDiffuse;
uniform vec3 lgSpecular;

uniform sampler2D TexSamplerColor;
uniform sampler2D TexSamplerNormal;

layout (location = 0) out vec4 fragColor;

vec3 diffuseAndSpecular(vec4 texColor, vec4 normal)
{
	vec4 mtDiffuse = texColor;
	vec3 n = normalize(vec3(normal));

	vec3 l = normalize (lDir);
	vec3 v = normalize (vDir);
	vec3 r = reflect(-l, n);

	vec3 diffuse = (lgDiffuse * vec3(mtDiffuse) * max(dot(l,n), 0.0));
	vec3 specular = (lgSpecular * vec3(mtSpecular) * pow(max(dot(r,v), 0.0), mtShininess));

	return diffuse + specular;
}

void main() 
{
	vec4 normal = (2.0 * texture(TexSamplerNormal, texCoord)) - 1.0;
	vec4 texColor = mtDiffuse * texture(TexSamplerColor, texCoord);
	fragColor = vec4(diffuseAndSpecular(texColor, normal), 1.0);
}