#version 400

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec3 vTangent;
layout (location = 3) in vec2 vTexCoord;

uniform mat4 mModelViewProj;
uniform mat4 mModelView;

uniform vec3 lgPosition;

uniform float dFactor;
uniform sampler2D TexSamplerEl;

out vec3 position;
out vec3 normal;
out vec3 lDir;
out vec3 vDir;
out vec2 texCoord;

void main()
{
	float displacement = texture(TexSamplerEl, vTexCoord).r;
	vec3 posAux = vPosition + (vNormal * displacement * dFactor);

	normal = vec3(mModelView * vec4(vNormal, 0.0));
	position = vec3(mModelView * vec4(posAux, 1.0));

	vec3 tangent = vec3(mModelView * vec4(vTangent, 0.0));
	vec3 binormal = normalize(cross(normal, tangent));

	mat3 TBN = transpose(mat3(tangent, binormal, normal));

	lDir = normalize(TBN * (lgPosition - position));
	vDir = normalize(TBN * (-position));

	texCoord = vTexCoord;
	gl_Position = mModelViewProj * vec4(posAux, 1.0);
}