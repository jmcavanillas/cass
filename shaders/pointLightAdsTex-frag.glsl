#version 400

in vec3 position;
in vec3 normal;
in vec2 texCoord;

uniform vec4 mtDiffuse;
uniform vec4 mtSpecular;
uniform float mtShininess;

uniform vec3 lgDiffuse;
uniform vec3 lgSpecular;
uniform vec3 lgPosition;

uniform sampler2D TexSamplerColor;

layout (location = 0) out vec4 fragColor;

vec3 diffuseAndSpecular(vec4 texColor)
{
	vec4 mtDiffuse = texColor;
	vec3 n = normalize(normal);

	vec3 l = normalize (lgPosition - position);
	vec3 v = normalize (-position);
	vec3 r = reflect(-l, n);

	vec3 diffuse = (lgDiffuse * vec3(mtDiffuse) * max(dot(l,n), 0.0));
	vec3 specular = (lgSpecular * vec3(mtSpecular) * pow(max(dot(r,v), 0.0), mtShininess));

	return diffuse + specular;
}

void main() 
{
	vec4 texColor = mtDiffuse * texture(TexSamplerColor, texCoord);
	fragColor = vec4(diffuseAndSpecular(texColor), 1.0);
}