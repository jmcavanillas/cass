#ifndef LINEALINTERPOL_H
#define LINEALINTERPOL_H

#include <vector>
#include <glm.hpp>

template <typename T>
class Lerp
{
private:
    static T basic_interpolation(const T& p1, const T& p2, double t);

    std::vector<T> points_;
    std::vector<double> times_;

public:
    Lerp();
    Lerp(const std::vector<T>& points, const std::vector<double>& times);

    T interpolate(double t) const;

    std::vector<T> points() const;
    void setPoints(const std::vector<T> &points);

    std::vector<double> times() const;
    void setTimes(const std::vector<double> &times);
};

template <typename T>
Lerp<T>::Lerp()
{
}

template <typename T>
T Lerp<T>::basic_interpolation(const T &p1, const T &p2, double t)
{
    return static_cast<float>(1-t) * p1 + static_cast<float>(t) * p2;
}

template <typename T>
std::vector<double> Lerp<T>::times() const
{
    return times_;
}

template <typename T>
void Lerp<T>::setTimes(const std::vector<double> &times)
{
    times_ = times;
}

template <typename T>
std::vector<T> Lerp<T>::points() const
{
    return points_;
}

template <typename T>
void Lerp<T>::setPoints(const std::vector<T> &points)
{
    points_ = points;
}

template <typename T>
Lerp<T>::Lerp(const std::vector<T> &points, const std::vector<double> &times)
{
    points_ = points;
    times_ = times;
}

template <typename T>
T Lerp<T>::interpolate(double t) const
{
    t = (t * 1000);

    if (t >= times_.back()) return basic_interpolation(points_[points_.size()-2], points_.back(), t/1000);
    if (t <= times_.front()) return basic_interpolation(points_[0], points_[1], t/1000);

    for (unsigned i = 0; i < times_.size() - 1; ++i)
    {
        // Buscamos el intervalo en el que esta
        if (t <= times_[i+1] && t >= times_[i])
        {
            //Calculamos el sub_t en el que esta
            double sub_t = (t - times_[i]) / (times_[i+1] - times_[i]);
            // LLamamos a la interpolación básica
            return basic_interpolation(points_[i], points_[i+1], sub_t);
        }
    }

    return points_.back();
}


#endif // LINEALINTERPOL_H
