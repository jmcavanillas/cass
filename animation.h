#ifndef ANIMATION_H
#define ANIMATION_H

#include <QTimer>
#include "linealinterpol.h"
#include "slerp.h"
#include "cubicbeziercurve2d.h"
#include "curveip.h"
#include "animable.h"

class Animation : public QObject
{
    Q_OBJECT

private:
    QTimer* timer_;

    bool anPosition_;
    bool anRotation_;
    bool anScale_;

    Slerp SphericalInterpolation_;
    Lerp<glm::vec3> posInterpolation_;
    CurveIP curveInterpolation_;

    bool usingSpeed_;
    CubicBezierCurve2D easing_;
    CubicBezierCurve2D speed_;

    Animable* object_;

    unsigned int interval_;
    unsigned int tickCount_;
    unsigned int dur_;

    double progress_;
public:
    Animation();

    void start();
    void setObject(Animable* object);

    virtual ~Animation();

    bool anPosition() const;
    void setAnPosition(bool anPosition);

    bool anRotation() const;
    void setAnRotation(bool anRotation);

    bool anScale() const;
    void setAnScale(bool anScale);

public slots:
    void update();
};

#endif // ANIMATION_H
