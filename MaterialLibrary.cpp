#include "MaterialLibrary.h"
#include "PAGTextureLibrary.h"

MaterialLibrary* MaterialLibrary::instance_ = nullptr;

MaterialLibrary::MaterialLibrary()
{
	PAGTextureLibrary* textures = PAGTextureLibrary::getInstance();

	// Materiales Predefinidos
	this->setMaterial("red", PAGMaterial(glm::vec4(1,0,0,1), glm::vec4(0, 0, 0, 1), 1));
	this->setMaterial("white", PAGMaterial(glm::vec4(1, 1, 1, 1), glm::vec4(0, 0, 0, 1), 1));
	this->setMaterial("black", PAGMaterial(glm::vec4(0.1, 0.1, 0.1, 1), glm::vec4(0.1, 0.1, 0.1, 1), 1));
	this->setMaterial("blue", PAGMaterial(glm::vec4(0, 0, 1, 1), glm::vec4(0, 0, 1, 1), 1));
	this->setMaterial("green", PAGMaterial(glm::vec4(0, 1, 0, 1), glm::vec4(0, 1, 0, 1), 1));

	PAGTexture* texture = textures->newTexture("test_texture");
    texture->addTexture("textures/uv_checker large.png");
	this->setMaterial("test_texture", PAGMaterial(glm::vec4(1, 1, 1, 1), glm::vec4(0.3, 0.3, 0.3, 1), 5, texture));

	texture = textures->newTexture("moon");
    texture->addTexture("textures/neutral.png");
    texture->addTexture("textures/moon_normals_2_720.png");
    texture->addTexture("textures/moon_el_map_2.png");
	this->setMaterial("moon", PAGMaterial(glm::vec4(0.96, 0.96, 1, 1), glm::vec4(0.03, 0.03, 0.03, 1), 1, texture));

	texture = textures->newTexture("metal");
    texture->addTexture("textures/tie_fighter.png");
    texture->addTexture("textures/tie_fighter_normal.png");
    texture->addTexture("textures/tie_fighter_el.png");

	this->setMaterial("metal_grey", PAGMaterial(glm::vec4(0.8, 0.8, 0.8, 1), glm::vec4(0.8, 0.8, 0.8, 1), 140, texture));

	texture = textures->newTexture("wing");
    texture->addTexture("textures/tie_fighter_wing.png");
    texture->addTexture("textures/tie_fighter_wing_normal.png");
    texture->addTexture("textures/tie_fighter_wing_el.png");
	this->setMaterial("crystal_black", PAGMaterial(glm::vec4(0.8, 0.8, 0.8, 1), glm::vec4(0.8, 0.8, 0.8, 1), 140, texture));

	texture = textures->newTexture("wing_center");
    texture->addTexture("textures/wing_lid.png");
    texture->addTexture("textures/wing_lid_normal.png");
    texture->addTexture("textures/wing_lid_el.png");
	this->setMaterial("wing_center", PAGMaterial(glm::vec4(0.8, 0.8, 0.8, 1), glm::vec4(0.8, 0.8, 0.8, 1), 140, texture));
}

MaterialLibrary * MaterialLibrary::getInstance()
{
	if (!instance_)
		instance_ = new MaterialLibrary();

	return instance_;
}

void MaterialLibrary::setMaterial(std::string name, const PAGMaterial& material)
{
	materials_.insert(std::pair<std::string, PAGMaterial>(name, material));
}

PAGMaterial MaterialLibrary::getMaterial(std::string name)
{
	return materials_[name];
}


MaterialLibrary::~MaterialLibrary()
{
}
