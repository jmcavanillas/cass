#ifndef SLERP_H
#define SLERP_H

#include <vector>
#include <glm.hpp>

class Slerp
{
private:
    static glm::dquat basic_interpolation(const glm::dquat& q1, const glm::dquat& q2, double t);

    std::vector<glm::dquat> dirs_;
    std::vector<double> times_;
public:
    Slerp();
    Slerp(const std::vector<glm::dquat>& points, const std::vector<double>& times);

    glm::quat interpolate(double t) const;

    std::vector<glm::dquat> dirs() const;
    void setDirs(const std::vector<glm::dquat> &dirs);

    std::vector<double> times() const;
    void setTimes(const std::vector<double> &times);
};

#endif // SLERP_H
