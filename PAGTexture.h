#pragma once

#include <GL/glew.h>
#include <string>
#include <vector>

class PAGTexture
{
	private:
		std::vector<GLuint> handlers_;

		void inverseImg(std::vector<unsigned char>& image, unsigned width, unsigned height);
		bool fileExists(const std::string& fileName);
	public:
		PAGTexture();

		GLuint addTexture(const std::string& fileName);
		void activate();

		void setTextureRepeat(bool repeat);
		void setTextureMinFilter(int filter);
		void setTextureMaxFilter(int filter);
		void setMirrorRepeat(bool mirror_repat);

		virtual ~PAGTexture();
};

