#ifndef BEZIERCURVE2D_H
#define BEZIERCURVE2D_H

#include <glm.hpp>
#include <vector>

class CubicBezierCurve2D
{
private:
    static glm::mat4 M;

    void fetch_polyline(unsigned frames);

    unsigned frames_;
    std::vector<glm::vec2> polyline_;
    std::vector<double> sLine_;

    glm::vec2 point1_;
    glm::vec2 point2_;
    glm::vec2 point3_;
    glm::vec2 point4_;

public:
    CubicBezierCurve2D();
    CubicBezierCurve2D(float v1, float v2, float v3, float v4);
    CubicBezierCurve2D(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3, const glm::vec2& p4);

    glm::vec2 getPoint (double t) const;

    glm::vec2 getTForX(double x) const;
    double searchForX(double s) const;
};

#endif // BEZIERCURVE2D_H
