#ifndef CUBICBEZIERCURVE1D_H
#define CUBICBEZIERCURVE1D_H

#include <glm.hpp>
#include <vector>

class CubicBezierCurve1D
{
private:
    double getSlope(double t) const;
    void fetchPolyline(unsigned frames);

    static glm::mat4 M;

    unsigned frames_;
    std::vector<double> arcLengths_;

    double val1_;
    double val2_;
    double val3_;
    double val4_;

public:

    CubicBezierCurve1D();
    CubicBezierCurve1D(double p1, double p2, double p3, double p4);

    double getVal(double t) const;
    double getTForX(double x) const;
    double searchArc(double s) const;
};

#endif // CUBICBEZIERCURVE1D_H
